FROM alpine:3.7
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser
ENTRYPOINT ["cat","/tmp/shadow"]
